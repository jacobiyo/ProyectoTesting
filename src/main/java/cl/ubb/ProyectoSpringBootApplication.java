package cl.ubb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cl.ubb.ProyectoSpringBootApplication;

@SpringBootApplication
public class ProyectoSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoSpringBootApplication.class, args);
	}
}
