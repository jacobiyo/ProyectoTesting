package cl.ubb.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Cliente {
	@Id
	@GeneratedValue
	private long id;
	private String celular;
	private String nombre;
	private String email;
	private String cat;
	public Cliente (){
		
	}

	public void setCelular(String celular) {
		// TODO Auto-generated method stub
		this.celular=celular;
		
	}

	public void setNombre(String nombre) {
		// TODO Auto-generated method stub
		this.nombre=nombre;
		
	}

	public void setId(long id) {
		
		// TODO Auto-generated method stub
		this.id=id;
	}

	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email=email;
	}
	public void setCat(String cat) {
		// TODO Auto-generated method stub
		this.cat=cat;
	}
	
	public String getCelular() {
		// TODO Auto-generated method stub
		return celular;		
	}

	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;		
	}

	public Long getId() {
			// TODO Auto-generated method stub
		return id;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}
	
	public String getCat() {
		// TODO Auto-generated method stub
		return cat;
	}

}
