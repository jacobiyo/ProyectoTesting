package cl.ubb.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity

public class TipoAuto {
	@Id 
	@GeneratedValue 
	private long id;
	private String nombre;
	private String tipotransmision;
	private String tipocombustible;
	private String cat;
	private int pasajeros;
	private int valordiario;
	private String a�o;
	private String marca;
	private String modelo;
	private String aireacon;
	private int bolsas;
	
	public void setId(long id) {
		this.id = id;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setTipoTransmision(String tipotransmision) {
		this.tipotransmision = tipotransmision;
	}
	public void setTipoCombustible(String tipocombustible) {
		this.tipocombustible = tipocombustible;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public void setPasajeros(int pasajeros) {
		this.pasajeros = pasajeros;
	}
	public void setValordiario(int valordiario) {
		this.valordiario = valordiario;
	}
	public void setA�o(String a�o) {
		this.a�o = a�o;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public void setAireacon(String aireacon) {
		this.aireacon = aireacon;
	}
	public void setBolsas(int bolsas) {
		this.bolsas = bolsas;
	}
	public long getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getTipoTransmision() {
		return tipotransmision;
	}
	public String getTipoCombustible() {
		return tipocombustible;
	}
	public String getCat() {
		return cat;
	}
	public int getPasajeros() {
		return pasajeros;
	}
	public int getValordiario() {
		return valordiario;
	}
	public String getA�o() {
		return a�o;
	}
	public String getMarca() {
		return marca;
	}
	public String getModelo() {
		return modelo;
	}
	public String getAireacon() {
		return aireacon;
	}
	public int getBolsas() {
		return bolsas;
	}
	
	public String obtenerListaTipo(ArrayList<TipoAuto> listaTipoAuto, String nombre) {
		String lista= "";
		for(int i=0; i<listaTipoAuto.size();i++){
			String cat= listaTipoAuto.get(i).getCat();
			if(cat==nombre ){
				lista= lista+listaTipoAuto.get(i).getId()+listaTipoAuto.get(i).getNombre()+listaTipoAuto.get(i).getTipoTransmision()+listaTipoAuto.get(i).getTipoCombustible()+listaTipoAuto.get(i).getPasajeros()+"|";
				System.out.println(lista);
			}

		}
		return lista;
	}
	
	public String obtenerListaAuto(ArrayList<TipoAuto> listaTipoAuto, Long id) {
		String lista= "";
		for(int i=0; i<listaTipoAuto.size();i++){
			Long id2= listaTipoAuto.get(i).getId();
			if(id2==id ){
				lista= lista+listaTipoAuto.get(i).getMarca()+listaTipoAuto.get(i).getModelo()+listaTipoAuto.get(i).getA�o()+"|";
				System.out.println(lista);
			}

		}
		return lista;
	}
}
