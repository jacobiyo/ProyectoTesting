package cl.ubb.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva {
	@Id 
	@GeneratedValue 
	private long id;
	private int id_cli;
	private int id_auto;
	private String fecha_ini;
	private String fecha_fin;
	private String cat;
	
	public void setId(long id) {
		this.id = id;
	}
	public void setId_cli(int id_cli) {
		this.id_cli = id_cli;
	}
	public void setId_auto(int id_auto) {
		this.id_auto = id_auto;
	}
	public void setFecha_ini(String fecha_ini) {
		this.fecha_ini = fecha_ini;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public long getId() {
		return id;
	}
	public int getId_cli() {
		return id_cli;
	}
	public int getId_auto() {
		return id_auto;
	}
	public String getFecha_ini() {
		return fecha_ini;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public String getCat() {
		return cat;
	}
	
	
	public String registrar (int idcliente, int idauto, String FechaI, String FechaT,String Cat){
	this.id_cli=idcliente;
	this.id_auto=idauto;
	this.fecha_ini=FechaI;
	this.fecha_fin=FechaT;
	this.cat=Cat;
	return(idcliente+","+idauto+","+fecha_ini+","+fecha_fin);
		
		
		
		
		
		
		
		
	}
	//test 8
	public String obtenerListaReservarAuto(ArrayList<Reserva> listaReserva, int id_cliente, String fecha_ini) {
		String lista= "";
		for(int i=0; i<listaReserva.size();i++){
			String fecha= listaReserva.get(i).getFecha_ini();
			if(fecha==fecha_ini ){
				lista= lista+listaReserva.get(i).getId_auto()+listaReserva.get(i).getFecha_ini()+listaReserva.get(i).getFecha_fin()+"|";
				System.out.println(lista);
			}

		}
		return lista;
	}
	
	
}