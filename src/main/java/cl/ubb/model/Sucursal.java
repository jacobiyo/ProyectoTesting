package cl.ubb.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Sucursal {
	@Id 
	@GeneratedValue 
	private long id;
	private String ciudad;
	public Sucursal(){		
	}
	
	public void setId(long id) {
		// TODO Auto-generated method stub
		this.id=id;		
	}

	public void setCiudad(String ciudad) {
		// TODO Auto-generated method stub
		this.ciudad=ciudad;		
	}
	
	public Long getId() {
		// TODO Auto-generated method stub
	return id;
    }

    public String getCiudad() {
	// TODO Auto-generated method stub
	return ciudad;
    }
    
	public String obtenerListaSucursal(ArrayList<Sucursal> listaSucursal) {
		String lista= "";
		for(int i=0; i<listaSucursal.size();i++){
				lista= lista+listaSucursal.get(i).getId()+listaSucursal.get(i).getCiudad()+"|";
				System.out.println(lista);
		}
		return lista;
	}
}
