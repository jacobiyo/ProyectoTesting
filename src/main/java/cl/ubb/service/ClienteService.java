package cl.ubb.service;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ClienteDao;

import cl.ubb.model.Cliente;
import java.util.ArrayList;
import java.util.List;
public class ClienteService {
	
	private final ClienteDao clienteDao;

	//inyectar clase
	@Autowired 
	public ClienteService(ClienteDao proyectoDao){
		this.clienteDao = proyectoDao;
	}

	public Cliente crearcliente(Cliente cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(cliente);
	}
	
	public Cliente obtenerCliente(Long id) {
		Cliente cliente=new Cliente();
		cliente=clienteDao.findOne(id);
		return cliente;
	}
	
	public String obtenerLista(ArrayList<Cliente> listaCliente, String nombre) {
		String lista= "";
		for(int i=0; i<listaCliente.size();i++){
			String cat= listaCliente.get(i).getCat();
			if(cat==nombre ){
				lista= lista+listaCliente.get(i).getId()+listaCliente.get(i).getNombre()+"|";
				System.out.println(lista);
			}

		}
		return lista;
	}
	
	



}
