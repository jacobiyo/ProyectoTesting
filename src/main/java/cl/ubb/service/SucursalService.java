package cl.ubb.service;
import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.SucursalDao;
import cl.ubb.model.Sucursal;
import java.util.ArrayList;
import java.util.List;
public class SucursalService {
	private final SucursalDao sucursalDao;
	@Autowired 
	public SucursalService(SucursalDao sucursaldao){
		this.sucursalDao = sucursaldao;
	}

	public Sucursal crearsucursal(Sucursal sucursal) {
		// TODO Auto-generated method stub
		return sucursalDao.save(sucursal);
	}
	
	public String obtenerListaSucursal(ArrayList<Sucursal> listaSucursal) {
		String lista= "";
		for(int i=0; i<listaSucursal.size();i++){
				lista= lista+listaSucursal.get(i).getId()+listaSucursal.get(i).getCiudad()+"|";
				System.out.println(lista);
		}
		return lista;
	}
}
