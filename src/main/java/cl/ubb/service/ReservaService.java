package cl.ubb.service;
import org.springframework.beans.factory.annotation.Autowired;


import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Reserva;


import java.util.ArrayList;
import java.util.List;

public class ReservaService {

	private final ReservaDao reservaDao;
	@Autowired 
	public ReservaService(ReservaDao reservadao){
		this.reservaDao = reservadao;
	}

	public Reserva crearreserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return reservaDao.save(reserva);
	}
}
