package cl.ubb.service;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.TipoAutoDao;
import cl.ubb.model.TipoAuto;

public class TipoAutoService {
	private final TipoAutoDao tipoautoDao;
	@Autowired 
	public TipoAutoService(TipoAutoDao tipoautodao){
		this.tipoautoDao = tipoautodao;
	}

	public TipoAuto creartipoauto(TipoAuto tipoauto) {
		// TODO Auto-generated method stub
		return tipoautoDao.save(tipoauto);
	}
}
