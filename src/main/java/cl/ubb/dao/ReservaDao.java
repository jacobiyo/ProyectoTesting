package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Reserva;

import org.springframework.data.jpa.repository.Query;

public interface ReservaDao extends CrudRepository <Reserva, Long>  {

}
