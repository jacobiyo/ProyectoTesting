package cl.ubb.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;
import cl.ubb.model.Sucursal;


public interface ClienteDao extends CrudRepository<Cliente, Long>{

	public Cliente findByTitulo(String titulo); 
	
	@Query("select id, nombre  FROM Cliente C where c.categoria = ?1")
	public Cliente findBycategory(String cat);
	


	
}