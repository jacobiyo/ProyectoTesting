package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

import cl.ubb.model.Sucursal;

public interface SucursalDao extends CrudRepository <Sucursal, Long> {

}
