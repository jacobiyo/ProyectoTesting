package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.TipoAuto;

import org.springframework.data.jpa.repository.Query;

public interface TipoAutoDao extends CrudRepository <TipoAuto, Long> {

}
