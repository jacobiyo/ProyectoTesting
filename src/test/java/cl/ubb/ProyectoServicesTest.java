package cl.ubb;

import static org.junit.Assert.*;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.dao.SucursalDao;
import cl.ubb.dao.TipoAutoDao;
import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.Sucursal;
import cl.ubb.model.TipoAuto;
import cl.ubb.model.Reserva;
import cl.ubb.service.ClienteService;
import cl.ubb.service.SucursalService;
import cl.ubb.service.TipoAutoService;
import cl.ubb.service.ReservaService;


@RunWith(MockitoJUnitRunner.class)
public class ProyectoServicesTest {
	@Mock 
	private ClienteDao proyectodao;
	private SucursalDao sucursaldao;
	private TipoAutoDao tipoautodao;
	private ReservaDao reservadao;
	
    @InjectMocks
    private ClienteService proyectoService;
   private SucursalService sucursalService;
   private TipoAutoService tipoautoService;
   private ReservaService reservaService;
    
	@Test
	public void RegistrarClienteDevolverNulo() {
		//arrange
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNombre("Pablo");
		cliente.setCelular("7484848");
		cliente.setEmail("pablo@asd.cl");
		cliente.setCat("particular");
		
		//act
		when(proyectodao.save(cliente)).thenReturn(cliente);
		Cliente clientecreado = proyectoService.crearcliente(cliente);
		
		//assert
		Assert.assertEquals(cliente,clientecreado);
		
	}

	
	@Test
	public void VerificarSiClienteSeEncuentraRegistrado(){
		//arrange
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNombre("Pablo");
		cliente.setCelular("7484848");
		cliente.setEmail("pablo@asd.cl");
		cliente.setCat("particular");
		//act
		when(proyectodao.findOne(anyLong())).thenReturn(cliente);
		Cliente ClienteRetornado=proyectoService.obtenerCliente(cliente.getId());
		//assert
		assertNotNull(ClienteRetornado);
	}
	
	@Test
	public void ListarCategor�asdeClientes() {
		//arange
		Cliente C1= new Cliente();
		Cliente C2= new Cliente();
		Cliente C3= new Cliente();
		C1.setId(1);
		C1.setNombre("Pablo");
		C1.setCelular("7484848");
		C1.setEmail("pablo@asd.cl");
		C1.setCat("particular");
		
		C2.setId(2);
		C2.setNombre("Jacob");
		C2.setCelular("7484848");
		C2.setEmail("jacob@asd.cl");
		C2.setCat("empresa");
		
		C3.setId(3);
		C3.setNombre("Macarena");
		C3.setCelular("7484848");
		C3.setEmail("macarena@asd.cl");
		C3.setCat("empresa");
		ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
		listaCliente.add(C1);
		listaCliente.add(C2);
		listaCliente.add(C3);
		String listaCat="2Jacob|3Macarena|";
		
		when(proyectodao.findAll()).thenReturn(listaCliente);//mock
		String resultado=proyectoService.obtenerLista(listaCliente, "empresa");

		Assert.assertEquals(listaCat,resultado);
		
	}

	@Test
	public void ListarSucursales() {
		//arrange
		//arrange
		Sucursal S1= new Sucursal();
		Sucursal S2= new Sucursal();
		Sucursal S3= new Sucursal();
		Sucursal SP= new Sucursal();
		S1.setId(1);
		S1.setCiudad("Concepcion");
		S2.setId(2);
		S2.setCiudad("Temuco");
		S3.setId(3);
		S3.setCiudad("Santiago");
		ArrayList<Sucursal> listaSucursal = new ArrayList<Sucursal>();
		listaSucursal.add(S1);
		listaSucursal.add(S2);
		listaSucursal.add(S3);
		String listaSuc="1Concepcion|2Temuco|3Santiago|";

		//when(proyectodao.findAll()).thenReturn(listaSucursal);//mock
		//String resultado=proyectoService.obtenerListaSucursal(listaSucursal);
		String resultado=SP.obtenerListaSucursal(listaSucursal);
		
		assertEquals(listaSuc,resultado);
	}
	
	 @Test
	 public void ListarTipoAutoPorCategoria() {
				TipoAuto TP1= new TipoAuto();
				TipoAuto TP2= new TipoAuto();
				TipoAuto TP3= new TipoAuto();
				TipoAuto TPP= new TipoAuto();
				TP1.setId(1);
				TP1.setNombre("Concepcion");
				TP1.setTipoTransmision("mec�nico");
				TP1.setTipoCombustible("diesel");
				TP1.setCat("Lujo");
				TP1.setPasajeros(4);
				TP1.setValordiario(400);
				TP1.setA�o("2000");
				TP1.setMarca("nissan");
				TP1.setModelo("modelo");
				TP1.setAireacon("Si");
				TP1.setBolsas(2);
				TP2.setId(2);
				TP2.setNombre("Concepcion");
				TP2.setTipoTransmision("automatio");
				TP2.setTipoCombustible("gasolina");
				TP2.setCat("transporte");
				TP2.setPasajeros(4);
				TP2.setValordiario(4);
				TP2.setA�o("2000");
				TP2.setMarca("nissan");
				TP2.setModelo("modelo");
				TP2.setAireacon("Si");
				TP2.setBolsas(2);
				TP3.setId(3);
				TP3.setNombre("Concepcion");
				TP3.setTipoTransmision("mecanico");
				TP3.setTipoCombustible("diesel");
				TP3.setCat("citycar");
				TP3.setPasajeros(4);
				TP3.setValordiario(4);
				TP3.setA�o("2000");
				TP3.setMarca("nissan");
				TP3.setModelo("modelo");
				TP3.setAireacon("Si");
				TP3.setBolsas(2);
				ArrayList<TipoAuto> listaTipoAuto = new ArrayList<TipoAuto>();
				listaTipoAuto.add(TP1);
				listaTipoAuto.add(TP2);
				listaTipoAuto.add(TP3);
				String listaTA="3Concepcionmecanicodiesel4|";

				//when(proyectodao.findAll()).thenReturn(listaSucursal);//mock
				//String resultado=proyectoService.obtenerListaSucursal(listaSucursal);
				String resultado=TPP.obtenerListaTipo(listaTipoAuto,"citycar");
				
				assertEquals(listaTA,resultado);
	}
	 
	 @Test
	 public void ListarAutoPorTipo() {
				TipoAuto TP1= new TipoAuto();
				TipoAuto TPP= new TipoAuto();
				TP1.setId(1L);
				TP1.setNombre("Concepcion");
				TP1.setTipoTransmision("mec�nico");
				TP1.setTipoCombustible("diesel");
				TP1.setCat("Lujo");
				TP1.setPasajeros(4);
				TP1.setValordiario(400);
				TP1.setA�o("2000");
				TP1.setMarca("nissan");
				TP1.setModelo("modelo");
				TP1.setAireacon("Si");
				TP1.setBolsas(2);
				ArrayList<TipoAuto> listaTipoAuto = new ArrayList<TipoAuto>();
				listaTipoAuto.add(TP1);
				String listaTA="nissanmodelo2000|";

				//when(proyectodao.findAll()).thenReturn(listaSucursal);//mock
				//String resultado=proyectoService.obtenerListaSucursal(listaSucursal);
				String resultado=TPP.obtenerListaAuto(listaTipoAuto, 1L);
				
				assertEquals(listaTA,resultado);
	}
	 
	 @Test
	 public void ListarReservasDeUnClienteDesdeUnaFecha() {
				Reserva R1= new Reserva();
				Reserva RP= new Reserva();
				R1.setId(1L);
				R1.setId_cli(1);
				R1.setId_auto(1);
				R1.setFecha_ini("27-03-2017");
				R1.setFecha_fin("06-04-2017");
				R1.setCat("citycar");
				ArrayList<Reserva> listaReserva = new ArrayList<Reserva>();
				listaReserva.add(R1);
				String listaRe="127-03-201706-04-2017|";

				//when(proyectodao.findAll()).thenReturn(listaSucursal);//mock
				//String resultado=proyectoService.obtenerListaSucursal(listaSucursal);
				String resultado=RP.obtenerListaReservarAuto(listaReserva, 1, "27-03-2017");
				
				assertEquals(listaRe,resultado);
	}
	 
		@Test
		public void RegistrarArriendoDevolverIdAutoyFechas() {
			//arrange
			Reserva r = new Reserva();
			String resultado="1,1,01-02-2017,01-03-2017";
			String R;
			
			//act
			R=r.registrar(1,1,"01-02-2017","01-03-2017","Lujo");
			//*when(reservadao.save(r)).thenReturn(r);
			//*Reserva reservacreada =reservaService.crearreserva(r);
			
			//assert
			assertEquals(R,resultado);
			
		}

	 
	
	 
	 
	 
	

}